//
//  File.swift
//  PennyPinch
//
//  Created by M Usman Bin Rehan on 06/01/2019.
//  Copyright © 2019 Travel. All rights reserved.
//

import Foundation
import UIKit

// MARK:- INDICATOR
class Utility{
    
    static func showLoader() {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let controller = storyboard.instantiateViewController(withIdentifier: "Loader")
        Utility.topViewController()?.present(controller, animated: false, completion: nil)
    }
    
    static func hideLoader() {
        Utility.topViewController()?.dismiss(animated: false, completion: nil)
    }
    
    static func showAlertPermission(message:String,title:String,controller:UIViewController, completionHandler: @escaping (UIAlertAction?, UIAlertAction?) -> Void){
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
    
        alertController.addAction(UIAlertAction(title: "Go to settings", style: UIAlertActionStyle.default){ (alertActionYES) in
            completionHandler(alertActionYES, nil)
        })
        
        alertController.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel){ (alertActionNO) in
            completionHandler(nil, alertActionNO)
        })
        
        controller.present(alertController, animated: true, completion: nil)
        
    }
    
    static func showAlert(message:String,title:String){
        if var topController = UIApplication.shared.keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
            alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            topController.present(alertController, animated: true, completion: nil)
        }
    }
    
    static func topViewController(base: UIViewController? = (AppDelegate.shared).window?.rootViewController) -> UIViewController? {
        
        if let nav = base as? UINavigationController {
            return topViewController(base: nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(base: selected)
            }
        }
        if let presented = base?.presentedViewController {
            return topViewController(base: presented)
        }
        return base
    }
}
extension NSObject {
    var className: String {
        return String(describing: type(of: self)).components(separatedBy: ".").last!
    }
    
    class var className: String {
        return String(describing: self).components(separatedBy: ".").last!
    }
}
