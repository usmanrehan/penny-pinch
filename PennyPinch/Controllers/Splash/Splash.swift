//
//  Splash.swift
//  PennyPinch
//
//  Created by M Usman Bin Rehan on 10/01/2019.
//  Copyright © 2019 Travel. All rights reserved.
//

import UIKit

class Splash: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        sleep(1)
        AppDelegate.shared.changeRootViewController()
        // Do any additional setup after loading the view.
    }

}
