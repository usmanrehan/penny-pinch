//
//  Loader.swift
//  PennyPinch
//
//  Created by M Usman Bin Rehan on 03/02/2019.
//  Copyright © 2019 Travel. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit

class Loader: UIViewController {
    
    @IBOutlet weak var videoPlayerView: UIView!
    //var avPlayer: AVPlayer!
    //var playerLayer:AVPlayerLayer!

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.playGif()
    }
}
extension Loader{
//    private func playVideo(){
//        guard let path = Bundle.main.path(forResource: "LoaderVideo", ofType:"gif") else {return}
//        let videoURL =  URL(fileURLWithPath: path)
//        self.avPlayer = AVPlayer(url: videoURL)
//        self.playerLayer = AVPlayerLayer(player: avPlayer)
//        self.playerLayer?.frame = CGRect(x:0, y: 0, width: UIScreen.main.bounds.size.width, height: self.videoPlayerView.frame.size.height)
//        self.playerLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
//        self.videoPlayerView.layer.addSublayer(self.playerLayer!)
//        self.avPlayer.play()
//
//        self.avPlayer?.actionAtItemEnd = .none
//
//        NotificationCenter.default.addObserver(self,
//                                               selector: #selector(playerItemDidReachEnd(notification:)),
//                                               name: .AVPlayerItemDidPlayToEndTime,
//                                               object: self.avPlayer?.currentItem)
//    }
//
//    @objc func playerItemDidReachEnd(notification: Notification) {
//        if let playerItem = notification.object as? AVPlayerItem {
//            playerItem.seek(to: kCMTimeZero, completionHandler: nil)
//        }
//    }

    private func playGif(){
        let jeremyGif = UIImage.gifImageWithName("Loader")
        let imageView = UIImageView(image: jeremyGif)
        imageView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        self.videoPlayerView.addSubview(imageView)
    }
}
