//
//  Welcome.swift
//  PennyPinch
//
//  Created by M Usman Bin Rehan on 06/01/2019.
//  Copyright © 2019 Travel. All rights reserved.
//

import UIKit
import OneSignal

class Welcome: UIViewController {

    @IBOutlet weak var btnEnableNotification: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(onDidNotificationStatus(_:)), name: Constants.notificationNameWelcome, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.checkOneSignalNotificationStatusAndNavigateToAppTabBar()
    }
    
    @IBAction func onBtnEnableNotifications(_ sender: UIButton) {
        OneSignal.promptForPushNotifications(userResponse: { accepted in
            print("User accepted notifications: \(accepted)")
            Constants.NotificationsOn = accepted
            if !accepted{
                Utility.showAlertPermission(message: Strings.switchOnNotifications.rawValue, title: Strings.notifications.rawValue, controller: self, completionHandler: { (yes, no) in
                    if yes != nil{
                        self.navigateToSettings()
                    }
                    if no != nil{
                        self.navigateToAppTabBar()
                    }
                })
            }
        })
    }
    @IBAction func onBtnNoThanks(_ sender: UIButton) {
        self.navigateToAppTabBar()
    }
    @IBAction func onBtnPrivacyPolicy(_ sender: UIButton) {
        self.navigateToWebViewContentLoader(title: "Privacy policy", urlString: Links.PrivacyPolicy.rawValue)
    }
    
    func navigateToAppTabBar(){
        let storyBoard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let controller = storyBoard.instantiateViewController(withIdentifier: "AppTabBar")
        self.navigationController?.pushViewController(controller, animated: true)
    }
    func navigateToWebViewContentLoader(title:String,urlString:String){
        let storyBoard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let controller = storyBoard.instantiateViewController(withIdentifier: "WebViewContentLoader") as! WebViewContentLoader
        controller.titleString = title
        controller.urlString = urlString
        self.navigationController?.pushViewController(controller, animated: true)
    }
    func navigateToSettings(){
        if let url = URL(string:UIApplicationOpenSettingsURLString) {
            if UIApplication.shared.canOpenURL(url) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }
    }
    func checkOneSignalNotificationStatusAndNavigateToAppTabBar(){
        let status = OneSignal.getPermissionSubscriptionState().permissionStatus.status.rawValue
        if status == 2{
            OneSignal.setSubscription(true)
            self.navigateToAppTabBar()
        }
    }
    @objc func onDidNotificationStatus(_ notification: Notification){
        self.checkOneSignalNotificationStatusAndNavigateToAppTabBar()
    }
}
