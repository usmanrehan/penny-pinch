//
//  Home.swift
//  PennyPinch
//
//  Created by M Usman Bin Rehan on 06/01/2019.
//  Copyright © 2019 Travel. All rights reserved.
//

import UIKit

class TabBarItemWithLinks: UIViewController {
    
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnShare: UIButton!
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var webviewTop: NSLayoutConstraint!
    @IBOutlet weak var navBar: UIView!
    
    var refController:UIRefreshControl = UIRefreshControl()
    var arrStringURLs = [String]()
    var urlString = ""
    var tapTime: TimeInterval = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tabBarController?.tabBar.isTranslucent = false
        
        self.webView.delegate = self
        
        self.refController.bounds = CGRect.init(x: 0.0, y: 0.0, width: refController.bounds.size.width, height: refController.bounds.size.height)
        self.refController.addTarget(self, action: #selector(self.mymethodforref(refresh:)), for: .valueChanged)
        self.refController.attributedTitle = NSAttributedString(string: Strings.PullDown.rawValue)
        self.webView.scrollView.addSubview(refController)
        
        self.loadURL()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        self.tabBarController?.delegate = self
    }
    @objc func mymethodforref(refresh:UIRefreshControl){
        self.loadURL()
        self.refController.endRefreshing()
    }
    
    @IBAction func onBtnBack(_ sender: UIButton) {
        self.arrStringURLs.removeLast()
        self.loadURL()
    }
    @IBAction func onBtnShare(_ sender: UIButton) {
        let shareText = self.arrStringURLs.last ?? ""
        let vc = UIActivityViewController(activityItems: [shareText], applicationActivities: [])
        present(vc, animated: true, completion: nil)
    }
}
extension TabBarItemWithLinks{
    private func loadURL(){
        
        let selectedTabBarIndex = self.tabBarController?.tabBar.items?.index(of: self.tabBarItem)
        
        switch selectedTabBarIndex {
        case 0:
            self.urlString = Links.Home.rawValue
        case 1:
            self.urlString = Links.Saved.rawValue
        case 2:
            self.urlString = Links.Stores.rawValue
        case 3:
            self.urlString = Links.Categories.rawValue
        default:
            return
        }
        if !self.arrStringURLs.contains(self.urlString){
            self.arrStringURLs.append(self.urlString)
        }
        if let url = URL(string: self.arrStringURLs.last ?? ""){
            let urlRequest = URLRequest(url: url)
            self.webView.loadRequest(urlRequest)
        }
        else{
            Utility.showAlert(message: Strings.InvalidURL.rawValue, title: Strings.Error.rawValue)
        }
    }
    private func loadRootURL(){
        self.arrStringURLs.removeAll()
        self.arrStringURLs.append(self.urlString)
        self.loadURL()
    }
}
extension TabBarItemWithLinks:UIWebViewDelegate{
    func webViewDidStartLoad(_ webView: UIWebView) {
        Utility.showLoader()
    }
    func webViewDidFinishLoad(_ webView: UIWebView) {
        if let currentURL = webView.request?.url{
            if self.urlString == currentURL.absoluteString{
                self.arrStringURLs.removeAll()
                self.arrStringURLs.append(self.urlString)
                self.btnBack.isHidden = true
                self.btnShare.isHidden = true
                self.navBar.isHidden = true
                self.webviewTop.constant = 0
            }
            else{
                if !self.arrStringURLs.contains(currentURL.absoluteString){
                    self.arrStringURLs.append(currentURL.absoluteString)
                    self.btnBack.isHidden = false
                    self.btnShare.isHidden = false
                    self.navBar.isHidden = false
                    self.webviewTop.constant = 50
                }
            }
        }
        if self.arrStringURLs.count <= 1{
            self.btnBack.isHidden = true
            self.btnShare.isHidden = true
        }
        Utility.hideLoader()
    }
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        Utility.hideLoader()
    }
}
extension TabBarItemWithLinks:UITabBarControllerDelegate{
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        let currentTime: TimeInterval = Date().timeIntervalSince1970
        let duration: TimeInterval = currentTime - tapTime
        self.tapTime = currentTime
        if duration < 0.35 {
            // double tap detected! write your code here
            print("Double tap")
            self.loadRootURL()
            self.tapTime = 0.0
            return
        }
        UIView.animate(withDuration: 0.45) {
            self.webView.stringByEvaluatingJavaScript(from: "window.scrollTo(0,0)")
        }
    }
}
