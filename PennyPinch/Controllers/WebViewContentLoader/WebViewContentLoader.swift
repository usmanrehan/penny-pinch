//
//  WebViewContentLoader.swift
//  PennyPinch
//
//  Created by M Usman Bin Rehan on 06/01/2019.
//  Copyright © 2019 Travel. All rights reserved.
//

import UIKit

class WebViewContentLoader: UIViewController {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var webView: UIWebView!
    
    var titleString = ""
    var urlString = ""
    var refController:UIRefreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.webView.delegate = self
        
        self.refController.bounds = CGRect.init(x: 0.0, y: 0.0, width: refController.bounds.size.width, height: refController.bounds.size.height)
        self.refController.addTarget(self, action: #selector(self.mymethodforref(refresh:)), for: .valueChanged)
        self.refController.attributedTitle = NSAttributedString(string: Strings.PullDown.rawValue)
        self.webView.scrollView.addSubview(refController)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.lblTitle.text = self.titleString
        self.loadURL()
    }
    @IBAction func onBtnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    private func loadURL(){
        if let url = URL(string: self.urlString){
            let urlRequest = URLRequest(url: url)
            self.webView.loadRequest(urlRequest)
        }
    }
    @objc func mymethodforref(refresh:UIRefreshControl){
        self.loadURL()
        self.refController.endRefreshing()
    }
}
extension WebViewContentLoader:UIWebViewDelegate{
    func webViewDidStartLoad(_ webView: UIWebView) {
        Utility.showLoader()
    }
    func webViewDidFinishLoad(_ webView: UIWebView) {
        Utility.hideLoader()
    }
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        Utility.hideLoader()
    }
}

