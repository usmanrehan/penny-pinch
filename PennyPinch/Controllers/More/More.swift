//
//  More.swift
//  PennyPinch
//
//  Created by M Usman Bin Rehan on 06/01/2019.
//  Copyright © 2019 Travel. All rights reserved.
//

import UIKit
import OneSignal

class More: UIViewController {
    
    @IBOutlet weak var switchNotificationStatus: UISwitch!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.switchNotificationStatus.isOn = Constants.NotificationsOn
        NotificationCenter.default.addObserver(self, selector: #selector(onDidNotificationStatus(_:)), name: Constants.notificationNameMore, object: nil)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        self.checkOneSignalNotificationStatusAndChangeSwitchStatus()
    }
    
    @IBAction func onBtnBack(_ sender: UIButton) {
        self.tabBarController?.selectedIndex = 0
    }
    @IBAction func onNotificationStatusChange(_ sender: UISwitch) {
        if sender.isOn{
            Utility.showAlertPermission(message: Strings.switchOnNotifications.rawValue, title: Strings.notifications.rawValue, controller: self, completionHandler: { (yes, no) in
                if yes != nil{
                    self.navigateToSettings()
                }
                if no != nil{
                    sender.isOn =  false
                }
            })
        }
        else{
            Utility.showAlertPermission(message: Strings.switchOffNotifications.rawValue, title: Strings.notifications.rawValue, controller: self, completionHandler: { (yes, no) in
                if yes != nil{
                    self.navigateToSettings()
                }
                if no != nil{
                    sender.isOn =  true
                }
            })
        }
    }
    @IBAction func onBtnBecomeAaMerchant(_ sender: UIButton) {
        self.navigateToWebViewContentLoader(title: "Become a merchant", urlString: Links.BecomeAMerchant.rawValue)
    }
    @IBAction func onBtnContactUs(_ sender: UIButton) {
        self.navigateToWebViewContentLoader(title: "Contact us", urlString: Links.ContactUs.rawValue)
    }
    @IBAction func onBtnPrivacyPolicy(_ sender: UIButton) {
        self.navigateToWebViewContentLoader(title: "Privacy policy", urlString: Links.PrivacyPolicy.rawValue)
    }
    @IBAction func onBtnTermsAndUse(_ sender: UIButton) {
        self.navigateToWebViewContentLoader(title: "Terms and use", urlString: Links.TermsAndUse.rawValue)
    }
    func navigateToWebViewContentLoader(title:String,urlString:String){
        let storyBoard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let controller = storyBoard.instantiateViewController(withIdentifier: "WebViewContentLoader") as! WebViewContentLoader
        controller.titleString = title
        controller.urlString = urlString
        self.navigationController?.pushViewController(controller, animated: true)
    }
    func navigateToSettings(){
        if let url = URL(string:UIApplicationOpenSettingsURLString) {
            if UIApplication.shared.canOpenURL(url) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }
    }
    func checkOneSignalNotificationStatusAndChangeSwitchStatus(){
        let status = OneSignal.getPermissionSubscriptionState().permissionStatus.status.rawValue
        if status == 2{
            self.switchNotificationStatus.isOn = true
            OneSignal.setSubscription(true)
        }
        else{
            self.switchNotificationStatus.isOn = false
            OneSignal.setSubscription(false)
        }
    }
    @objc func onDidNotificationStatus(_ notification: Notification){
        self.checkOneSignalNotificationStatusAndChangeSwitchStatus()
    }
}
