//
//  AppDelegate.swift
//  PennyPinch
//
//  Created by M Usman Bin Rehan on 06/01/2019.
//  Copyright © 2019 Travel. All rights reserved.
//

import UIKit
import OneSignal
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    static let shared: AppDelegate = UIApplication.shared.delegate as! AppDelegate

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        let onesignalInitSettings = [kOSSettingsKeyAutoPrompt: false]
        
        // Replace 'YOUR_APP_ID' with your OneSignal App ID.
        OneSignal.initWithLaunchOptions(launchOptions,
                                        appId: "0091aaf8-fa7f-431b-8c2d-b97110a71e22",
                                        handleNotificationAction: {(result)
                                            in
                                            if let aps = result?.notification.payload.rawPayload{
                                                self.getPayload(hashAPS: aps)
                                            }
        },
                                        settings: onesignalInitSettings)
        
        OneSignal.inFocusDisplayType = OSNotificationDisplayType.notification;
        
        FirebaseApp.configure()
        
        // Recommend moving the below line to prompt for push after informing the user about
        //   how your app will use them.
//        OneSignal.promptForPushNotifications(userResponse: { accepted in
//            print("User accepted notifications: \(accepted)")
//        })
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        if Utility.topViewController()?.className == Constants.Welcome{
            NotificationCenter.default.post(name: Constants.notificationNameWelcome, object: nil)
        }
        if Utility.topViewController()?.className == Constants.More{
            NotificationCenter.default.post(name: Constants.notificationNameMore, object: nil)
        }
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

}

extension AppDelegate{
    func changeRootViewController(){
        let status = OneSignal.getPermissionSubscriptionState().permissionStatus.status.rawValue
        let isLoaded = UserDefaults.standard.bool(forKey: "isLoaded")
        if status == 2 || isLoaded{
            Constants.NotificationsOn = true
            OneSignal.setSubscription(true)
            self.showHome()
        }
        else{
            UserDefaults.standard.set(true, forKey: "isLoaded")
            Constants.NotificationsOn = false
            OneSignal.setSubscription(false)
            self.showNotificationPrompt()
        }
    }
    func showHome(){
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let navigationController = UINavigationController(rootViewController: storyboard.instantiateViewController(withIdentifier: "AppTabBar"))
        navigationController.navigationBar.isHidden = true
        self.window?.rootViewController = navigationController
    }
    func showNotificationPrompt(){
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let navigationController = UINavigationController(rootViewController: storyboard.instantiateViewController(withIdentifier: "Welcome"))
        navigationController.navigationBar.isHidden = true
        self.window?.rootViewController = navigationController
    }
}

extension AppDelegate{
    func getPayload(hashAPS:Dictionary<AnyHashable, Any>){
        guard let aps = hashAPS["aps"] as? NSDictionary else {return}
        if let alert = aps["alert"] as? NSDictionary{
            guard let title = alert["title"] as? String else {return}
            guard let urlString = alert["body"] as? String else {return}
            guard let _ = URL(string: urlString) else {return}
            self.navigateToWebViewContentLoader(title: title, urlString: urlString)
        }
    }
    func navigateToWebViewContentLoader(title:String,urlString:String){
        let storyBoard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let controller = storyBoard.instantiateViewController(withIdentifier: "WebViewContentLoader") as! WebViewContentLoader
        controller.titleString = title
        controller.urlString = urlString
        Utility.topViewController()?.navigationController?.pushViewController(controller, animated: true)
    }
}
