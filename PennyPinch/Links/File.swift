//
//  File.swift
//  PennyPinch
//
//  Created by M Usman Bin Rehan on 06/01/2019.
//  Copyright © 2019 Travel. All rights reserved.
//

import Foundation

enum Links:String{
    
    case Home = "https://www.pennypinch.deals/?app=yes"
    case Saved = "https://www.pennypinch.deals/favourites/?app=yes"
    case Stores = "https://www.pennypinch.deals/stores/?app=yes"
    case Categories = "https://www.pennypinch.deals/voucher_category/?app=yes"
    case ContactUs = "https://www.pennypinch.deals/contact-us/?app=yes"
    case BecomeAMerchant = "https://www.pennypinch.deals/merchants/?app=yes"
    case PrivacyPolicy = "https://www.pennypinch.deals/privacy-policy/?app=yes"
    case TermsAndUse = "https://www.pennypinch.deals/terms-of-use/?app=yes"
    
}

