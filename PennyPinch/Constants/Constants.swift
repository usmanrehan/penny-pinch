//
//  File.swift
//  PennyPinch
//
//  Created by M Usman Bin Rehan on 10/01/2019.
//  Copyright © 2019 Travel. All rights reserved.
//

import Foundation
struct Constants {
    
    static var NotificationsOn = false
    static let Welcome = "Welcome"
    static let More = "More"
    static let notificationNameWelcome = Notification.Name("Welcome")//Welcome screen
    static let notificationNameMore = Notification.Name("More")//More screen
}
