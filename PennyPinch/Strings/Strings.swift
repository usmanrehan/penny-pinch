//
//  Strings.swift
//  PennyPinch
//
//  Created by M Usman Bin Rehan on 10/01/2019.
//  Copyright © 2019 Travel. All rights reserved.
//

import Foundation

enum Strings:String{
    case Error = "Error"
    case InvalidURL = "Invalid link"
    case notifications = "Notifications"
    case switchOnNotifications = "You have cancelled the notification permission before, kindly go to app settings and open Notifications and Background App Refresh in order to get alerts for our latest deals."
    case switchOffNotifications = "To turn off notifications, kindly go to app settings and close Notifications and Background App Refresh in order to discontinue alerts of our latest deals."
    case PullDown = "Pull down to refresh"
}
